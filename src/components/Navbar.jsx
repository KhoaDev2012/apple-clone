import React from "react";
import { appleImg, bagImg, searchImg } from "../utils";
import { navLists } from "../constants";

const Navbar = () => {
  return (
    <header className="w-full py-5 sm:px-10 px-5 flex items-center justify-between">
      <nav className="w-full flex screen-max-width">
        <img src={appleImg} alt="logo" width={30} height={30} />

        <div className="max-sm:hidden flex flex-1 justify-center">
          {navLists.map((nav, index) => (
            <div
              key={index}
              className="px-5 text-md cursor-pointer text-gray hover:text-white transition-all"
            >
              {nav}
            </div>
          ))}
        </div>

        <div className="flex items-baseline gap-7 max-sm:justify-end max-sm:flex-1">
          <img src={searchImg} alt="search" width={20} height={20} />
          <img src={bagImg} alt="bag" width={20} height={20} />
        </div>
      </nav>
    </header>
  );
};

export default Navbar;
